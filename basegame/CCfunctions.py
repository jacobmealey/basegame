import os 
import sys

def init_text():

    os.system('clear')
    print(" ____    __    ___  ____  ___    __    __  __  ____") 
    print("(  _ \  /__\  / __)( ___)/ __)  /__\  (  \/  )( ___)")
    print(" ) _ < /(__)\ \__ \ )__)( (_-. /(__)\  )    (  )__)") 
    print("(____/(__)(__)(___/(____)\___/(__)(__)(_/\/\_)(____)")
    print("      By: Jacob Mealey")
    print("")


def prompt() :
    sys.stdout.write('<You> ')
    sys.stdout.flush()
