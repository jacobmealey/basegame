# BASEGAME CHAT SERVER 
# created by Jacob Mealey

 
import sys
import socket
import select
import json
import random
import os

HOST = '' 
SOCKET_LIST = []
RECV_BUFFER = 4096 
PORT = 9009

inventoryItems = []
os.system('clear')
class bcolors:
    #text colors
    HEADER = '\033[0m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[31m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# A function for pulling non alphabetic characters
# from a string
def get_num(x):
    return float(''.join(ele for ele in x if ele.isdigit() or ele == '.'))	

#Adds items to inventory list
def inventory(item):
    inventoryItems.append(item)

def autonomyJson(monsterEvent):
    #opens that 
    with open('meta.json') as json_data:
        #d is now the array with all of the json data
        d = json.load(json_data)
        print(d)
        monster1 = d['monsters']['monster1'] 
        # monsterxMin is used as the minimum amount that can be used
        # by randomNum, where randomNum is a random number between
        # 0 and monsterxMax and if random number is less then 
        # monsterMin then the event will happen 
        monster1min = int(monster1['min'])
        monster1max = int(monster1['whole'])
        monsterEvent = monster1['event']
        randomNum = random.randrange(0, monster1max)
        if randomNum <= monster1min:
            print randomNum
            print ("Its good!")
            print (monsterEvent)
            return(monsterEvent)
        else:
            return None
def chat_server():

    #socket initiation

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(10)
 
    # add server socket object to the list of readable connections
    SOCKET_LIST.append(server_socket)
 
    print "Chat server started on port " + str(PORT)
 
    while 1:

        # get the list sockets which are ready to be read through
        ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST,[],[],0)
      
        for sock in ready_to_read:
            # a new connection request recieved
            if sock == server_socket: 
                sockfd, addr = server_socket.accept()
                SOCKET_LIST.append(sockfd)
                print "Client (%s, %s) connected" % addr
                broadcast(server_socket, sockfd, "[%s:%s] entered our chatting room\n" % addr)
             
            # A new message from the client, do this when not a new connection
            else:
                # process data recieved from client, 
                try:
			data = sock.recv(RECV_BUFFER)

			#name system
			x = data
			n = x.split(':')
			name = [n[0]] + [' '+l for l in n[0:]]
			print name[1]
                        #finds what is said after the ':' 
			d = x.split(' ')
                        print(d)  

			if data:
				#spits out what user put in
				broadcast(server_socket, sock, bcolors.HEADER+"\r" + x)
				#For trouble shooting				
			if d[1] == 'h\n':
				#outputs when input is "h"
				broadcast(server_socket, sock, bcolors.OKGREEN+"\r"+"user "+ name[0] +" isn't aware of the rules\n"+bcolors.HEADER)
                                print("almost")
			if d[1] == 'a\n':
				#outputs when input is "a"
				broadcast(server_socket,sock, bcolors.WARNING+"\r"+"*!* user "+name[0]+" is attacking *!*\n"+bcolors.HEADER)

			if d[1] == 'm\n':
				#outputs when input is "m"
    				broadcast(server_socket,sock, "\r"+"*!* user "+name[0]+" has moved the group forward *!*\n")
                                monster = ""
                                monevent = autonomyJson(monster)
                                print monevent
                                broadcast(server_socket,sock, "\r"+ monevent + "\n")
			if d[1] == 'mb\n':
				#outputs when input is "mb"
				broadcast(server_socket,sock,"\r"+"*!* user "+name[0]+" has moved the group backwards *!*\n")

			if d[1] == 'i\n':
				#outputs when input is "i"
                                broadcast(server_socket,sock, "\r Inventory: \n") 
                                #list everything in the inventory
                                for x in inventoryItems:
                                    print x
                                    broadcast(server_socket,sock, "-"+x+"\n")

                                    
			if d[1] == 'i+':
                                #adds items to the inventory
                                thing = d[2].rstrip()
	                        inventory(thing)

                                broadcast(server_socket,sock,"\r"+"*!* user "+name[0]+" has added "+ thing +" to the inventory\n")


                # exception 
                except:
                    #is able to disconnect one client without crashing server 
                    print("disconncted") 
                    SOCKET_LIST.remove(sockfd)

# broadcast chat messages to all connected clients
def broadcast (server_socket, sock, message):
    for socket in SOCKET_LIST:
        # send the message only to peer
        if socket != server_socket and socket != sock:
            try:
                socket.send(message)
            except:
                # broken socket connection
                socket.close()
                # broken socket, remove it
                if socket in SOCKET_LIST:
                    SOCKET_LIST.remove(socket)
if __name__ == "__main__":

    sys.exit(chat_server())  
